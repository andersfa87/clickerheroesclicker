﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace mouseclicker2
{
    public class Program
    {
        static void Main(string[] args)
        {
            IntPtr window = Win32Api.FindWindowByCaption(IntPtr.Zero, "Clicker Heroes");
            RECT windowSize = new RECT();

            if (window.ToInt32() != 0 && Win32Api.GetWindowRect(window, out windowSize))
            {
                int width = windowSize.Right - windowSize.Left;
                int height = windowSize.Bottom - windowSize.Top;

                int xPos = (int)(width * 0.75);
                int yPos = (int)(height * 0.5);

                Start(new HandleRef(null, window), xPos, yPos, windowSize.Left, windowSize.Top, width, height);
            }
            else
            {
                Console.WriteLine("Could not get window");
            }

        }

        private static void Start(HandleRef window, int xPos, int yPos, int windowx, int windowy, int width, int height)
        {
            var timer = new Timer(25);
            timer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                if (Win32Api.PostMessage(window, Win32Api.WM_LBUTTONDOWN, IntPtr.Zero, Win32Api.MakeLParam(xPos, yPos)))
                {
                    Win32Api.PostMessage(window, Win32Api.WM_LBUTTONUP, IntPtr.Zero, Win32Api.MakeLParam(xPos, yPos));
                }
            };
            timer.AutoReset = true;
            timer.Start();
            bool running = true;

            var fishtimer = new Timer
            {
                Interval = 5000,
                AutoReset = false
            };
            fishtimer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                using (var bmp = new Bitmap(width, height))
                using (var graphics = Graphics.FromImage(bmp))
                {
                    graphics.CopyFromScreen(windowx, windowy, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);

                    Console.WriteLine("Checking for fish");

                    bool doBreak = false;
                    for (int x = 1000; x < bmp.Width && !doBreak; x++)
                    {
                        for (int y = 400; y < bmp.Height && !doBreak; y++)
                        {
                            var pixel = bmp.GetPixel(x, y);

                            if (IsApproxemately(pixel.R, 247) && IsApproxemately(pixel.G, 223) && IsApproxemately(pixel.B, 178))
                            {
                                Console.WriteLine("hit dat fish, yo !: x: " + x + " - y: " + y);
                                if (Win32Api.PostMessage(window, Win32Api.WM_LBUTTONDOWN, IntPtr.Zero, Win32Api.MakeLParam(x, y)))
                                {
                                    Win32Api.PostMessage(window, Win32Api.WM_LBUTTONUP, IntPtr.Zero, Win32Api.MakeLParam(x, y));
                                }
                                //doBreak = true;
                            }
                        }
                    }

                    Console.WriteLine("Done checking for fish");
                    fishtimer.Start();
                }
            };
            fishtimer.Start();
            Console.WriteLine("fish termnator is in rape mode, interval is: " + fishtimer.Interval);

            while (true)
            {
                Console.ReadLine();
                if (running)
                {
                    timer.Stop();
                    Console.WriteLine("Clicker paused");
                }
                else
                {
                    timer.Start();
                    Console.WriteLine("Clicker started");
                }
                running = !running;
            }
        }

        public static bool IsApproxemately(byte value, int approx)
        {
            return InRange(value, approx - 3, approx + 3);
        }

        public static bool InRange(byte value, int left, int right)
        {
            return value >= left && value <= right; 
        }
    }

    public class Win32Api
    {
        public static uint WM_LBUTTONDOWN = 0x0201;
        public static uint WM_LBUTTONUP = 0x0202;


        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(HandleRef hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        // For Windows Mobile, replace user32.dll with coredll.dll
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        // Find window by Caption only. Note you must pass IntPtr.Zero as the first parameter.

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        public static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hwnd, out RECT lpRect);

        public static IntPtr MakeLParam(int LoWord, int HiWord)
        {
            return new IntPtr((HiWord << 16) | (LoWord & 0xffff));
        }


    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left;        // x position of upper-left corner
        public int Top;         // y position of upper-left corner
        public int Right;       // x position of lower-right corner
        public int Bottom;      // y position of lower-right corner
    }
}
